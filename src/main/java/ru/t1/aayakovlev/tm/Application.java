package ru.t1.aayakovlev.tm;

import ru.t1.aayakovlev.tm.constant.ArgumentConstant;
import ru.t1.aayakovlev.tm.constant.CommandConstant;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        processArgument(args);

        processCommand();
    }

    private static void processCommand() {
        System.out.println("**Welcome to Task Manager!**");
        Scanner scanner = new Scanner(System.in);
        String command;

        while(!Thread.currentThread().isInterrupted()) {
            System.out.println("Enter command:");
            command = scanner.nextLine();

            switch (command) {
                case CommandConstant.ABOUT:
                    showAbout();
                    break;
                case CommandConstant.VERSION:
                    showVersion();
                    break;
                case CommandConstant.HELP:
                    showCommandHelp();
                    break;
                case CommandConstant.EXIT:
                    showExit();
                default:
                    showCommandError();
                    break;
            }
        }
    }

    private static void processArgument(final String[] arguments) {
        if (arguments == null || arguments.length == 0 || arguments[0] == null || arguments[0].isEmpty()) return;
        String argument = arguments[0];

        switch (argument) {
            case ArgumentConstant.ABOUT:
                showAbout();
                break;
            case ArgumentConstant.VERSION:
                showVersion();
                break;
            case ArgumentConstant.HELP:
                showArgumentHelp();
                break;
            default:
                showArgumentError();
                break;
        }
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("email: aayakovlev@t1-consulting.ru");
        System.out.println("name: Yakovlev Anton.");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    private static void showArgumentHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - show developer info.\n", ArgumentConstant.ABOUT);
        System.out.printf("%s - show application version.\n", ArgumentConstant.VERSION);
        System.out.printf("%s - show arguments description.\n", ArgumentConstant.HELP);
    }
    private static void showCommandHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - show developer info.\n", CommandConstant.ABOUT);
        System.out.printf("%s - show application version.\n", CommandConstant.VERSION);
        System.out.printf("%s - show command description.\n", CommandConstant.HELP);
        System.out.printf("%s - exit program.\n", CommandConstant.EXIT);
    }

    private static void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("Passed argument not recognized...");
        System.err.println("Try 'java -jar task-manager.jar -h' for more information.");
        System.exit(1);
    }

    private static void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("Passed command not recognized...");
        System.err.println("Try 'help' for more information.");
    }

    private static void showExit() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

}
