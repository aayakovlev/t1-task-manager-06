# TASK MANAGER

## DEVELOPER

**NAME**: Yakovlev Anton

**E-MAIL**: aayakovlev@t1-consulting.ru

## SOFTWARE

**OS**: Windows 10 LTSC 1809 (build 17763.2628)

**JDK**: Oracle Java 1.8.0_301

**MAVEN**: 3.6.3

## HARDWARE

**CPU**: AMD R9

**RAM**: 32 Gb

**SSD**: NVMe 512 Gb

## BUILD APPLICATION

````shell
mvn clean install
````

## RUN APPLICATION

````shell
java -jar ./task-manager.jar
````
